from setuptools import setup, find_packages


setup(
    name='inkmeasure',
    version='1.0',
    packages=find_packages(exclude=['tests*']),
    license='MIT',
    description='InK Measure as Python Package',
    long_description=open('README.md').read(),
    install_requires=[],
    url='https://gitlab.com/h2t/kit-mseg/inkmeasure',
    author='Christian R. G. Dreher',
    author_email='c.dreher@kit.edu'
)
