from typing import Any, Dict, List, Optional, Tuple

from inkmeasure import binary


KeyFrame = int
ClassIdentifier = Any


class EvaluationResults:
  def __init__(self):
    self.class_wise_results: Dict[ClassIdentifier, binary.EvaluationResults] = {}
    self.micro = binary.EvaluationResults()
    self.macro = binary.EvaluationResults()
    self.weighted = binary.EvaluationResults()


def calc_error_areas(gt: List[Tuple[KeyFrame, ClassIdentifier]], seg: List[Tuple[KeyFrame, ClassIdentifier]], classes: List[ClassIdentifier], frame_count: int, sigma: float) -> Tuple[Dict[ClassIdentifier, binary.ErrorAreas], Dict[ClassIdentifier, int]]:
  a: Dict[ClassIdentifier, binary.ErrorAreas] = {}
  weights: Dict[ClassIdentifier, int] = {}

  for cl in classes:
    gt_single = [x[0] for x in gt if x[1] == cl]
    seg_single = [x[0] for x in seg if x[1] == cl]
    a[cl] = binary.calc_error_areas(gt_single, seg_single, frame_count=frame_count, sigma=sigma)
    weights[cl] = len(gt_single)

  return a, weights


def evaluate(a_dict: Dict[ClassIdentifier, binary.ErrorAreas], weights: Dict[ClassIdentifier, int]) -> EvaluationResults:
  """
  Implemented according to scikit-learn: https://scikit-learn.org/stable/modules/model_evaluation.html#precision-recall-f-measure-metrics
  :param a_dict: Dict of error areas for each class
  :return: Evaluation results for given error areas
  """

  assert len(a_dict) == len(weights)

  classes = a_dict.keys()

  r = EvaluationResults()

  # Evaluate class-wise first.
  for cl, a in a_dict.items():
    r.class_wise_results[cl] = binary.evaluate(a)

  # Evaluate micro scores.
  bin_a = sum(a_dict.values())
  r.micro = binary.evaluate(bin_a)

  # Evaluate macro scores.
  r.macro = (1. / len(classes)) * sum(r.class_wise_results.values())

  # Evaluate weighted scores.
  r.weighted = (1. / sum(weights.values())) * sum([weights[cl] * r.class_wise_results[cl] for cl in classes])

  return r
